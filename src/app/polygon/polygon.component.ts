import { Component, OnInit, ViewChild } from '@angular/core';
import { AgmPolygon } from '@agm/core';

@Component({
  selector: 'app-polygon',
  templateUrl: './polygon.component.html',
  styleUrls: ['./polygon.component.css']
})
export class PolygonComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  onPathChanged($event){
    console.log('$event', $event.then((e)=>{
      
      e.map((item, index) => {
        console.log(item.lat(), item.lng());
      })

    }));
  }



  

  lat:number = 54.0354;
  lng:number = 24.0354;

  Path_lat = 55.032649;
  Path_lng = 24.032649;

  paths = [
    {lat:this.Path_lat,lng:this.Path_lng},
    {lat:54.984988,lng:24.265448},
    {lat:54.3201548,lng:24.03298522},
  ]


  Increase(){
    this.paths[0].lat++;
    console.log(this.paths);
  }

  Decrease(){
    this.Path_lat--;
  }



  RadiusChange(event){
    console.log(event);
  }
  CenterChange(event){
    console.log(event)
  }
}
