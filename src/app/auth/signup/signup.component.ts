import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
 
//OR if you have initial value you could use following code
import * as moment from 'jalali-moment';
import { AuthService } from '../auth.service';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  dateObject :any;
  
  constructor(
    private authService:AuthService
    
  ) { 
  }


  ngOnInit() {
  }
  onSubmit(form:NgForm) {
    this.authService.registerUser({
      email:form.value.email,
      password:form.value.password
    }) 
  }

}
