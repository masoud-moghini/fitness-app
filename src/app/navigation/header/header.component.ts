import { Component, OnInit, Output,EventEmitter,OnDestroy} from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit ,OnDestroy{

  constructor(
    private authService :AuthService
  ) { }

  @Output() SidenavToggler=new EventEmitter<void>();
  isAuth:boolean;
  authSubscription : Subscription;
  ToggleSidenav(){
    this.SidenavToggler.emit();
  }
  ngOnInit() {
    this.authSubscription = this.authService.authChange.subscribe(authStatus=>{
      this.isAuth = authStatus;
    })
  }


  onLogout()
  {
    this.authService.logout();
  }
  ngOnDestroy(){
    this.authSubscription.unsubscribe();
  }

}
