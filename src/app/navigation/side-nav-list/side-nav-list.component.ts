import { Component, OnInit, EventEmitter } from '@angular/core';
import { Output } from '@angular/core';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-side-nav-list',
  templateUrl: './side-nav-list.component.html',
  styleUrls: ['./side-nav-list.component.css']
})
export class SideNavListComponent implements OnInit {

  constructor(private authService:AuthService) { }

  @Output() SidenavClosed =new EventEmitter<void>();
  closeSidenav(){
    this.SidenavClosed.emit();
  }
  onLogout(){
    this.closeSidenav();
    this.authService.logout();
  }
  ngOnInit() {
  }

}
