import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { StopTrainingComponent } from './stop-training.component';
import { TrainingService } from '../training.service';

@Component({
  selector: 'app-current-training',
  templateUrl: './current-training.component.html',
  styleUrls: ['./current-training.component.css']
})
export class CurrentTrainingComponent implements OnInit {

  constructor(
    private dialog :MatDialog,
    private trainingService:TrainingService
  ) { }

  timer:number;
  progressed = 0;
  ngOnInit() {
    const step = this.trainingService.GetRunningExercise().duration*10;// devided by 100 multiplied by 1000
    this.timer = setInterval(() => {
      this.progressed+=1
      if(this.progressed==101)
        {
          clearInterval(this.timer);
          this.trainingService.completeExercise();
        }
    }, 200)
  }
OnStop(){
  clearInterval(this.timer);
  const dialogRef = this.dialog.open(StopTrainingComponent,{data:{
    progressed : this.progressed
  }});
  dialogRef.afterClosed().subscribe(res=>{
    console.log(res)
    if(res){
      this.trainingService.cancelExercise(this.progressed);
    }
  })  
}
  
}
