import { Component, OnInit } from '@angular/core';
import {PastTrainingComponent} from './past-training/past-training.component'
import {NewTrainingComponent} from './new-training/new-training.component'
import { TrainingService } from './training.service';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.css']
})
export class TrainingComponent implements OnInit {

  constructor(
    private  trainingService:TrainingService
  ) { }

  ongoingTraining:boolean=false;
  subscription:Subscription;
  ngOnInit() {
    this.subscription = this.trainingService.exerciseChanged.subscribe(
      exercise=>{
        if(exercise)
          this.ongoingTraining=true;
        
        else{
          this.ongoingTraining=false;
        }
      }
    )
    
  }
  StopTraining(){
    this.ongoingTraining = false;
    console.log(this.ongoingTraining)
  }


}
