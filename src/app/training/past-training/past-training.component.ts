import { Component, OnInit,ViewChild, AfterViewInit } from '@angular/core';
import { MatTableDataSource ,MatSort, MatPaginator} from '@angular/material';
import { Exercise } from 'src/app/training/exercise.model';
import { TrainingService } from '../training.service';

@Component({
  selector: 'app-past-training',
  templateUrl: './past-training.component.html',
  styleUrls: ['./past-training.component.css']
})
export class PastTrainingComponent implements OnInit,AfterViewInit {

  constructor(
    private trainingService:TrainingService
  ) {
    this.dataSource.data=this.trainingService.GetCompletedOrCanceledExercises();
   }

   @ViewChild(MatSort) sort:MatSort;
   @ViewChild(MatPaginator) paginator:MatPaginator;
  displayedColumns=[
    "date",
    "name",
    "duration",
    "calories",
    "state"
  ]
  dataSource=new MatTableDataSource<Exercise>()
  ngOnInit() {
  }

  ngAfterViewInit(){
    this.dataSource.sort = this.sort;
    this.dataSource.paginator=this.paginator;
  }
  doFilter(filteredValue:string){
    this.dataSource.filter=filteredValue.trim().toLowerCase();
  }

}
