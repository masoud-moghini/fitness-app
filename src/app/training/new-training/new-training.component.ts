import { Component, OnInit ,EventEmitter,Output} from '@angular/core';
import { TrainingService } from '../training.service';
import { Exercise } from '../exercise.model';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-new-training',
  templateUrl: './new-training.component.html',
  styleUrls: ['./new-training.component.css']
})
export class NewTrainingComponent implements OnInit {

  @Output() startTraining =new EventEmitter<void>();
  StartTraining(form:NgForm){
    this.TrainingService.startExercise(form.value.exercise);
  }
  exersices : Exercise[];
  constructor(
    public TrainingService:TrainingService
  ) { }

  ngOnInit() {
    this.exersices=this.TrainingService.getAvailableExercise()
  }

}
