import { Exercise } from "./exercise.model";
import { Subject } from "rxjs";
export class TrainingService{
    availableExercise:Exercise[]=[
        {id:"crunches",name:"crunches",duration:30,calories:8},
        {id:"touch-toe",name:"Touch Toe",duration:180,calories:5},
        {id:"side-lungs",name:"Side Lungs",duration:150,calories:7},
        {id:"burpees",name:"Burpees",duration:51,calories:1}
    ]
    private runningExercise:Exercise;
    public exerciseChanged:Subject<Exercise>=new Subject();
    public exercises :Exercise[]=[];
    getAvailableExercise(){
        return this.availableExercise.slice();
    }
    startExercise(selected_exercise:string){
        this.runningExercise = this.availableExercise.find(ex=>ex.id==selected_exercise)
        this.exerciseChanged.next(this.runningExercise);
    }
    completeExercise(){
        this.exercises.push(
            {
                ...this.runningExercise,
                date:new Date(),
                state:'completed'
            });
        this.runningExercise=null;
        this.exerciseChanged.next(null);
    }
    cancelExercise(progress:number){
        this.exercises.push(
            {
                ...this.runningExercise,
                duration:this.runningExercise.duration*(progress/100),
                calories:this.runningExercise.calories*(progress/100),
                date:new Date(),
                state:'canceled'
            });
        this.runningExercise=null;
        this.exerciseChanged.next(null);
    }
    GetRunningExercise(){
        return {...this.runningExercise}
    }
    GetCompletedOrCanceledExercises(){
        return this.exercises.slice();
    }
}