// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDwg8Vfa-o8TLzXSJHWLZwZpzU0zErdCQo",
    authDomain: "ng-fitness-tracking-c2e1f.firebaseapp.com",
    databaseURL: "https://ng-fitness-tracking-c2e1f.firebaseio.com",
    projectId: "ng-fitness-tracking-c2e1f",
    storageBucket: "ng-fitness-tracking-c2e1f.appspot.com",
    messagingSenderId: "956520796510"
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
